#define SSD1306_NO_SPLASH 

#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include <Fonts/FreeSansBold9pt7b.h>

// pin definitions
int MOSFET_Pin[4]={5, 4, 3, 2};
int Bat_Pin[4]={A1, A3, A4, A6};
int Res_Pin[4]={A0, A2, A5, A7};
#define BUZZER 7
#define BUTTON 6

// voltage ranges for each chemistry
#define MIN_V_NIMH 0.9 
#define MAX_V_NIMH 1.6 
#define MIN_V_LIION 2.9
#define MAX_V_LIION 4.3

// tester states
#define STATE_IDLE 0
#define STATE_ERROR 1
#define STATE_DISCHARGING 2
#define STATE_DISCHARGED 3

// battery chemistry types
#define TYPE_NIMH 0
#define TYPE_LIION 1

// button state
#define BUTTON_PRESSED LOW
#define BUTTON_RELEASED HIGH

int state[4]={STATE_IDLE, STATE_IDLE, STATE_IDLE, STATE_IDLE};
int type[4]={TYPE_LIION, TYPE_LIION, TYPE_LIION, TYPE_LIION};

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_DC     9 // hardware SPI0
#define OLED_CS     10
#define OLED_RESET  8
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT,
  &SPI, OLED_DC, OLED_RESET, OLED_CS);

float Capacity[4] = {0.0, 0.0, 0.0, 0.0};
float Res_Value = 95.3/20;
float Vcc = 5.010; //Measure it and change
float Current[4] = {0.0, 0.0, 0.0, 0.0};
float mA[4] = {0, 0, 0, 0};
float Bat_Volt[4] = {0.0, 0.0, 0.0, 0.0};
float Res_Volt[4] = {0.0, 0.0, 0.0, 0.0};
unsigned long previousMillis[4] = {0, 0, 0, 0};;
unsigned long millisPassed[4] = {0, 0, 0, 0};
float sample1 = 0.000;
float sample2 = 0.000;
float MAX_V[4]={MAX_V_LIION, MAX_V_LIION, MAX_V_LIION, MAX_V_LIION};
float MIN_V[4]={MIN_V_LIION, MIN_V_LIION, MIN_V_LIION, MIN_V_LIION};
int channel=0;
int btnState;
int lastBtnState=BUTTON_RELEASED;
unsigned long lastDebounceTime=0;
unsigned long debounceDelay=50;

void draw(void)
{
  display.clearDisplay();
  display.setTextColor(SSD1306_WHITE); // Draw white text
  display.setTextSize(1);      // Normal 1:1 pixel scale
  display.setFont(&FreeSansBold9pt7b);
  if (state[channel]==STATE_IDLE)
  {
    display.setCursor(10, 40);
    display.println("No Battery");
  }
  else if (state[channel]==STATE_ERROR && Bat_Volt[channel] > MAX_V_LIION)
  {
    display.setCursor(3, 40);
    display.println("High Voltage");
  }
  else if (state[channel]==STATE_ERROR && Bat_Volt[channel] < MIN_V_NIMH)
  {
    display.setCursor(3, 40);
    display.println("Low Voltage");
  }
  else 
  {
    display.setCursor(0, 10);
    display.print("V: ");
    display.setCursor(0, 27);
    display.print("I: ");
    display.setCursor(0, 42);
    display.print("mAh: ");
    display.setCursor(58, 12);
    display.print(Bat_Volt[channel], 2);
    display.println("V");
    display.setCursor(58, 29);
    display.print(mA[channel], 0);
    display.println("mA");
    display.setCursor(58, 44);
    display.print(Capacity[channel], 1);

    display.setCursor(0, 60);
    display.print((state[channel]==STATE_DISCHARGING)?"Testing":"Done");
    display.setCursor(75, 60);
    display.print((type[channel]==TYPE_LIION)?"Li":"Ni");
  }
  display.setCursor(102, 60);
  display.print(String(channel+1).c_str());

  display.display();
}

void setup()
{
  for (int i=0; i<4; i++)
  {
    pinMode(MOSFET_Pin[i], OUTPUT);
    digitalWrite(MOSFET_Pin[i], LOW);
  }
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(BUZZER, OUTPUT);
  
  if (!display.begin(SSD1306_SWITCHCAPVCC))
  {
    tone(BUZZER, 110, 500);
    for(;;);
  }
  display.clearDisplay();
  display.display();

  tone(BUZZER, 925, 100);
}

void loop()
{
  // cycle through all channels
  for (int ch=0; ch<4; ch++)
  {
    // read voltages
    for (int i = 0; i < 100; i++)
    {
      sample1 = sample1 + analogRead(Bat_Pin[ch]);
      delay(2);
    }
    sample1 = sample1 / 100;
    Bat_Volt[ch] = 2 * sample1 * Vcc / 1024.0;

    for (int i = 0; i < 100; i++)
    {
      sample2 = sample2 + analogRead(Res_Pin[ch]);
      delay(2);
    }
    sample2 = sample2 / 100;
    Res_Volt[ch] = 2 * sample2 * Vcc / 1024.0;

    // handle state transitions
    if (state[ch]==STATE_IDLE && Bat_Volt[ch]>=MIN_V_NIMH && Bat_Volt[ch]<MAX_V_LIION)
    {
      state[ch]=STATE_DISCHARGING;
      if (Bat_Volt[ch]>=MIN_V_LIION)
      {
        type[ch]=TYPE_LIION;
        MAX_V[ch]=MAX_V_LIION;
        MIN_V[ch]=MIN_V_LIION;
      }
      else
      {
        type[ch]=TYPE_NIMH;
        MAX_V[ch]=MAX_V_NIMH;
        MIN_V[ch]=MIN_V_NIMH;
      }
      
      digitalWrite(MOSFET_Pin[ch], HIGH);
      Capacity[ch]=0;
    }
    if (state[ch]==STATE_IDLE && (Bat_Volt[ch]>=MAX_V[ch] || (Bat_Volt[ch]>=0.1 && Bat_Volt[ch]<MIN_V[ch])))
    {
      state[ch]=STATE_ERROR;
      digitalWrite(MOSFET_Pin[ch], LOW);
      Current[ch]=0;
      mA[ch]=0;
      tone(BUZZER, 110, 500);
    }
    if (state[ch]==STATE_DISCHARGING && Bat_Volt[ch]<=MIN_V[ch])
    {
      state[ch]=STATE_DISCHARGED;
      digitalWrite(MOSFET_Pin[ch], LOW);
      Current[ch]=0;
      mA[ch]=0;
      tone(BUZZER, 925, 100);
    }
    if (state[ch]!=STATE_IDLE && Bat_Volt[ch]<0.1)
    {
      state[ch]=STATE_IDLE;
      digitalWrite(MOSFET_Pin[ch], LOW);
      Current[ch]=0;
      mA[ch]=0;
    }

    // if discharging, update capacity
    if (state[ch]==STATE_DISCHARGING)
    {
      millisPassed[ch] = millis() - previousMillis[ch];
      Current[ch] = (Bat_Volt[ch] - Res_Volt[ch]) / Res_Value;
      mA[ch] = Current[ch] * 1000.0;
      Capacity[ch] = Capacity[ch] + mA[ch] * (millisPassed[ch] / 3600000.0);
      previousMillis[ch] = millis();
      delay(100);
    }
  }

  // check for button press, change channel if pressed
  int btnread=digitalRead(BUTTON);
  if (btnread!=lastBtnState)
    lastDebounceTime=millis();
  if ((millis()-lastDebounceTime)>debounceDelay)
  {
    if (btnread!=btnState)
      btnState=btnread;
    if (btnState==BUTTON_PRESSED)
    {
      channel++;
      if (channel==4)
        channel=0;
    }
  }
  lastBtnState=btnread;

  // update display
  draw();
}
